!   To compile:
!   gfortran  server.f90 -o server.x -L"../../../libmsock/libmsock/" -lmsock
!   change path to libmsock to reflect your system

PROGRAM MSockets_server
    USE ISO_C_BINDING  !use this for ability to use c code
    USE MSockets  ! this provides the bindings to the c functions for TCP

    implicit none

    character(72) ::  process
!   .....................Initializations for TCP..............................
    INTEGER(C_SHORT)                    :: port
    INTEGER(C_INT)                      :: sockfd
    CHARACTER(KIND=C_CHAR, LEN=102400)  :: buffer
    INTEGER(CC_SIZE_T)                  :: count
    INTEGER(C_INT)                      :: length, error

!   ..........................Begin Processes.................................

!   Opening the socket
    port=HUGE(port) ! Most likely unused port
    sockfd=ServerSocket(port,2_c_int) ! It will return only once a connection is made
    IF(sockfd<0) STOP "Failed to open server socket"

    write(6,*) "Simulator Connected!"

    do
        length = sockGets(sockfd,buffer,int(len(buffer),CC_SIZE_T))
        if(length<0) exit
        if(length>0) then
            read(buffer,*) process  ! interpretting data sent into buffer
            write(6,*) process
            write(6,*) process == 'test'
            stop
        endif
    enddo

    stop
END PROGRAM !program