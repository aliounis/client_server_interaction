! To compile
! gfortran client.f90 -o client.x -L"../../../libmsock/libmsock/" -lmsock

PROGRAM MSockets_Client
   USE ISO_C_BINDING
   USE MSockets
   IMPLICIT NONE
   
   INTEGER(C_SHORT) :: port
   INTEGER(C_INT) :: sockfd
   CHARACTER(KIND=C_CHAR, LEN=1024) :: buffer
   INTEGER(CC_SIZE_T) :: count
   INTEGER(C_INT) :: length, error
     
   port=HUGE(port) ! Most likely unused port
   sockfd=ClientSocket("localhost"//C_NULL_CHAR,port)
   IF(sockfd<0) STOP "Failed to open client socket"
   
   WRITE(*,*) "Enter what you want to tell the server:"
   DO
      READ(*,"(A)") buffer
      IF(buffer=="quit") EXIT ! Last input
      error=sockPuts(sockfd,TRIM(buffer)//C_NEW_LINE)
      IF(error<0) STOP "Server died!"
   END DO

   error=CloseSocket(sockfd)
   IF(sockfd<0) STOP "Failed to close client socket"   
   
END PROGRAM
