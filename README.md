To compile please be sure you have installed the library from: http://www.muquit.com/muquit/software/libmsock/libmsock.html first.

After installing the libmsock library you can follow the compilation instructions at the top of each file.

Be sure to compile sockets.f90 first as it is required to compile server and client.

Be sure to start server first and then start client in a separate terminal window.



